package com.example.tenthtask

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.PagerAdapter
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.RawValue


class CustomPagerAdapter(private val myContext: Context) : PagerAdapter() {

    lateinit var layoutView : View

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val modelObject = Model.values()[position]
        val inflater = LayoutInflater.from(myContext)
        val layout = inflater.inflate(modelObject.layoutResId, container, false) as View
        layoutView = layout
        layout.setOnClickListener {
            openFragment2()
        }
        container.addView(layout)
        return layout
    }

    private fun openFragment2(){
        val fragment = Fragment1()
        val image : ImageView = layoutView.findViewById(R.id.image)
        val imageModel = Image(image)
        val action = Fragment1Directions.transition(imageModel)
        fragment.findNavController().navigate(action)
    }

    override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
        collection.removeView(view as View)
    }

    override fun getCount(): Int {
        return Model.values().size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

}