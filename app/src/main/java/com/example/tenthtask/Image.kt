package com.example.tenthtask

import android.os.Parcelable
import android.widget.ImageView
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.RawValue

@Parcelize
data class Image(val image : @RawValue ImageView) : Parcelable {

}
