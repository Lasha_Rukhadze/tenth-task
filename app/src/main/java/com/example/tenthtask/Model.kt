package com.example.tenthtask

enum class Model (val layoutResId : Int) {
    FIRST(R.layout.first_layout),
    SECOND(R.layout.second_layout),
    THIRD(R.layout.third_layout),
    FOURTH(R.layout.fourth_layout)
}